import React from "react";
import "./Educationup.css";


const Educationup = () => {
    return (
    <div className="container" id={'education'}>
    <div className="container_midle employee_row">
        <div className="box_item">
            <div className="title_blocks">
                Обучение
            </div>
            <div className="text_row">
                Выпускники высших учебных заведений по престижным и востребованным на современном рынке
                направлениям «Мехатроника и Робототехника», «Механика и математическое моделирование»,
                «Обработка металлов давлением», «Технологии современного сварочного производства». Так же,
                Мы, никогда не останавливаемся на достигнутом наши специалисты получают дополнительное
                образование по автоматизации от таких гигантов автоматизации как: OMRON, Universal Robots и
                Yaskawa Motoman.
            </div>
        </div>
        <div className="box_item">
            <img src="https://cdn.dribbble.com/users/2125057/screenshots/6370627/hand2.gif" alt="employees" className="employee_img" />
        </div>
    </div>
</div>
    );
};

export default Educationup;