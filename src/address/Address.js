import React from "react";
import "./Address.css";
import epsilanwmin from "../images/EpsilanWhiteMini-min.png";
// import map from "../images/map-min.png";
import call from "../images/1_call-min.png";
import mail from "../images/2_mail-min.png";
// import tg from "../images/3_tg-min.png";
// import inst from "../images/4_inst-min.png";

const Address = () => {
    return (
        <div className="bg_address" id={'address'}>
        <div className="container container_midle">

            <div className="address_row">

                {/* <div className="contacts_row"> */}

                    <div className="address_container">
                        <div className="title_contact">
                            Контакты
                        </div>
                        <div className="text_row">
                            Адрес: г. Екатеринбург, ул. Амундсена дом 105, этаж 4
                        </div>
                        <img src={epsilanwmin} alt="Epsilan" width="165px"/>
                    </div>
                    {/* </div> */}



                    <div className="address_container">
                        <div className="block">
                        <iframe src="https://yandex.ru/map-widget/v1/?um=constructor%3A08342ddf80c80f846d20aa5434ab05141d01433e096c7299122cd0b1bce10286&amp;source=constructor" width="500" height="240" frameborder="0" title="map"></iframe>
                        </div>
                    </div>


                    <div className="address_container flex_row_nav_col ">

                        {/* <div className="numbers_phone"> */}
                            <div className="address_row">
                                    <div className="text_number">
                                        +7(343)-287-92-25
                                    </div>
                                    <div>
                                        <img src={call} alt="call" className="icons"/>
                                    </div>
                            </div>

                        <div className="address_row">
                            <div className="text_number">
                            ao-epsilon@mail.ru
                            </div>
                            <div>
                                <img src={mail} alt="mail" className="icons"/>
                            </div>
                        </div>

                        {/* <div className="address_row">
                            <div className="text_number">
                                @zavod_inno_tech
                            </div>
                            <div>
                                <img src={tg} alt="tg" className="icons"/>
                            </div>
                        </div>

                        <div className="address_row">
                            <div className="text_number">
                                zavod_inno_tech
                            </div>
                            <div>
                                <img src={inst} alt="inst" className="icons"/>
                            </div>
                        </div> */}


                    </div>


                </div>
            </div>
        </div>
//    </div>
    );
};

export default Address;