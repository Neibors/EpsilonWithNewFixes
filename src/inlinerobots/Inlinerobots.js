import React from "react";
// import "./Inlinerobots.css";
import robotsinline from "../images/RobotsinLine-min.png";

const Inlinerobots = () =>  {
    return (
        <div className="inline_robots_bg">
                <img src={robotsinline} alt="RobotsinLine" className="img_robotsinline"  />
        </div>
    );
};

export default Inlinerobots;