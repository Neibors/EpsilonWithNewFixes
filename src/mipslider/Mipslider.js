import React from "react";
// Import Swiper React components
import { Swiper, SwiperSlide } from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

import "./styles.css";

// import required modules
import { Autoplay, Pagination, Navigation } from "swiper";

export default function App() {
  return (
    <>
      <Swiper
        spaceBetween={30}
        centeredSlides={true}
        autoplay={{
          delay: 2500,
          disableOnInteraction: false,
        }}
        pagination={{
          clickable: true,
        }}
        navigation={true}
        modules={[Autoplay, Pagination, Navigation]}
        className="mySwiper"
      >
        <SwiperSlide>
            Осуществить дополнительную
            образовательную деятельность, которая
            позволит выпускать наиболее подготовленные
            кадры к реальным рабочим условиям по
            направлениям связанным с сварочным
            производством, автоматизации, а так же
            показать возможные варианты своего призвания
            в социальной рабочей среде для подрастающего
            поколения.
        </SwiperSlide>
        <SwiperSlide>
                    Переквалифицировать уже имеющиеся кадры, тем самым совместив опыт полученный в реальной рабочей деятельности с требованиями, которые диктует развитие автоматизации промышленности.
        </SwiperSlide>
                <SwiperSlide>
                    Вести разработку инновационных решений для различного типов оборудования, процессов промышленности, автоматизации и т.д.
                </SwiperSlide>
            </Swiper>
    </>
  );
};


