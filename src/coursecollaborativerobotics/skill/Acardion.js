import React from "react";


const Acardion = ({active, setActive}) => {
    return (       
        <div className={active ? "dropdown dropdownActive" : "dropdown"}>
            <div className="text_row">
                <ul>
                    <li>&#8226; Базовые понятия коллаборативности и принципа работы с роботом UR</li>
                    <li>&#8226; Основы структуры и функции системы робота UR</li>
                    <li>&#8226; Обзор механики робота UR, контроллера робота, использование пульта управления</li>
                    <li>&#8226; Навыки управления коллаборативным роботом UR, особенности работы с свободным приводом</li>
                    <li>&#8226; Использование пользовательских сообщений и движений в системе PolyScope</li>
                </ul>
            </div>
        </div>
    );
    };
    export default Acardion;