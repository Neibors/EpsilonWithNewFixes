import React, { useState } from "react";
import "./Training.css";

const Training = () => {
    const [Robotics, setRobotics] = useState(false);
    const [IndustrialRobotics, setIndustrialRobotics] = useState(false);
    const [IndustrialRoboticsProgres, setIndustrialRoboticsProgres] =
        useState(false);
    const [СollaborativeRobotics, setСollaborativeRobotics] = useState(false);
    const [СollaborativeRoboticsProgres, setСollaborativeRoboticsProgres] =
        useState(false);

    return (
        <div className="graybg">
            <div className="container">
                <div className="container_midle flex_row_nav">
                    <div
                        onMouseOver={() => setRobotics(true)}
                        onMouseLeave={() => setRobotics(false)}
                    >
                        {Robotics ? (
                            <div className="column_courses_white">
                                <div className="up_text_column_courses_black">
                                    «Промышленная робототехника»
                                </div>
                                <div className="down_text_column_courses_black">
                                    уровень | Стартовый
                                </div>
                            </div>
                        ) : (
                            <div className="column_courses_black">
                                <div className="up_text_column_courses_white">
                                    «Промышленная робототехника»
                                </div>
                                <div className="down_text_column_courses_white">
                                    уровень | Стартовый
                                </div>
                            </div>
                        )}
                    </div>

                    <div
                        onMouseOver={() => setIndustrialRobotics(true)}
                        onMouseLeave={() => setIndustrialRobotics(false)}
                    >
                        {IndustrialRobotics ? (
                            <div className="column_courses_white">
                                <div className="up_text_column_courses_black">
                                    «Промышленная робототехника»
                                </div>
                                <div className="down_text_column_courses_black">
                                    уровень | Стартовый
                                </div>
                            </div>
                        ) : (
                            <div className="column_courses_black">
                                <div className="up_text_column_courses_white">
                                    «Промышленная робототехника»
                                </div>
                                <div className="down_text_column_courses_white">
                                    уровень | Стартовый
                                </div>
                            </div>
                        )}
                    </div>

                    <div
                        onMouseOver={() => setIndustrialRoboticsProgres(true)}
                        onMouseLeave={() => setIndustrialRoboticsProgres(false)}
                    >
                        {IndustrialRoboticsProgres ? (
                            <div className="column_courses_white">
                                <div className="up_text_column_courses_black">
                                    «Промышленная робототехника»
                                </div>
                                <div className="down_text_column_courses_black">
                                    уровень | продвинутый
                                </div>
                            </div>
                        ) : (
                            <div className="column_courses_black">
                                <div className="up_text_column_courses_white">
                                    «Промышленная робототехника»
                                </div>
                                <div className="down_text_column_courses_white">
                                    уровень | продвинутый
                                </div>
                            </div>
                        )}
                    </div>

                    <div
                        onMouseOver={() => setСollaborativeRobotics(true)}
                        onMouseLeave={() => setСollaborativeRobotics(false)}
                    >
                        {СollaborativeRobotics ? (
                            <div className="column_courses_white">
                                <div className="up_text_column_courses_black">
                                    «Промышленная робототехника»
                                </div>
                                <div className="down_text_column_courses_black">
                                    уровень | Стартовый
                                </div>
                            </div>
                        ) : (
                            <div className="column_courses_black">
                                <div className="up_text_column_courses_white">
                                    «Промышленная робототехника»
                                </div>
                                <div className="down_text_column_courses_white">
                                    уровень | Стартовый
                                </div>
                            </div>
                        )}
                    </div>

                    <div
                        onMouseOver={() =>
                            setСollaborativeRoboticsProgres(true)
                        }
                        onMouseLeave={() =>
                            setСollaborativeRoboticsProgres(false)
                        }
                    >
                        {СollaborativeRoboticsProgres ? (
                            <div className="column_courses_white">
                                <div className="up_text_column_courses_black">
                                    «Коллаборативная робототехника»
                                </div>
                                <div className="down_text_column_courses_black">
                                    уровень | продвинутый
                                </div>
                            </div>
                        ) : (
                            <div className="column_courses_black">
                                <div className="up_text_column_courses_white">
                                    «Коллаборативная робототехника»
                                </div>
                                <div className="down_text_column_courses_white">
                                    уровень | продвинутый
                                </div>
                            </div>
                        )}
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Training;
{
    /*    <div className="container_education">
                        <div className="title_blocks_middle">обучение</div>
                        <div 
                            onMouseOver={() => setIsShowDefferentText(true)}
                            onMouseLeave={() => setIsShowDefferentText(false)}
                            className="text_row_middle">
                            Разработаны основные положения для обучения детей,
                            так и студентов высших учебных заведений и уже
                            готовых кадров в сфере автоматизации производства.
                            Для примера ниже показана блок схема приобретаемых
                            навыков после прохождения курса для детей от 6-9лет.
                        </div>
                    </div> */
}
