import React, { useState } from "react";
import "./Trainingbutt.css";

const Training = (props) => {
    const { onChange } = props;
    // props.onChange ...
    // const Trainingbutt = ({ onChange }) => { ...

    const [robotics, setRobotics] = useState(false);
    const [industrialRobotics, setIndustrialRobotics] = useState(false);
    const [industrialRoboticsProgres, setIndustrialRoboticsProgres] =
        useState(false);
    const [collaborativeRobotics, setСollaborativeRobotics] = useState(false);
    const [collaborativeRoboticsProgres, setСollaborativeRoboticsProgres] =
        useState(false);

    const handlerClick = (course) => {
        onChange(course);
        setRobotics((robstay1) => robstay1);
        setIndustrialRobotics((robstay2) => robstay2);
        setIndustrialRoboticsProgres((robstay3) => robstay3);
        setСollaborativeRobotics((robstay4) => robstay4);
        setСollaborativeRoboticsProgres((robstay5) => robstay5);
    };

    return (
        <>
            <div className="graybg">
                <div className="container">
                    <div className="blocks_container flex_row_nav_cul">
                        <div
                            onClick={() => handlerClick("first")}
                            onMouseOver={() => setRobotics(true)}
                            onMouseLeave={() => setRobotics(false)}
                        >
                            {robotics ? (
                                <div className="column_courses_white">
                                    <div className="up_text_column_courses_black">
                                        «Робототехника» для детей от 6-9 лет
                                    </div>
                                    <div className="down_text_column_courses_black">
                                        уровень | Стартовый
                                    </div>
                                </div>
                            ) : (
                                <div className="column_courses_black">
                                    <div className="up_text_column_courses_white">
                                        «Робототехника» для детей от 6-9 лет
                                    </div>
                                    <div className="down_text_column_courses_white">
                                        уровень | Стартовый
                                    </div>
                                </div>
                            )}
                        </div>

                        <div
                            onClick={() => handlerClick("second")}
                            onMouseOver={() => setIndustrialRobotics(true)}
                            onMouseLeave={() => setIndustrialRobotics(false)}
                        >
                            {industrialRobotics ? (
                                <div className="column_courses_white">
                                    <div className="up_text_column_courses_black">
                                        «Промышленная робототехника»
                                    </div>
                                    <div className="down_text_column_courses_black">
                                        уровень | Стартовый
                                    </div>
                                </div>
                            ) : (
                                <div className="column_courses_black">
                                    <div className="up_text_column_courses_white">
                                        «Промышленная робототехника»
                                    </div>
                                    <div className="down_text_column_courses_white">
                                        уровень | Стартовый
                                    </div>
                                </div>
                            )}
                        </div>

                        <div
                            onClick={() => handlerClick("third")}
                            onMouseOver={() =>
                                setIndustrialRoboticsProgres(true)
                            }
                            onMouseLeave={() =>
                                setIndustrialRoboticsProgres(false)
                            }
                        >
                            {industrialRoboticsProgres ? (
                                <div className="column_courses_white">
                                    <div className="up_text_column_courses_black">
                                        «Промышленная робототехника»
                                    </div>
                                    <div className="down_text_column_courses_black">
                                        уровень | продвинутый
                                    </div>
                                </div>
                            ) : (
                                <div className="column_courses_black">
                                    <div className="up_text_column_courses_white">
                                        «Промышленная робототехника»
                                    </div>
                                    <div className="down_text_column_courses_white">
                                        уровень | продвинутый
                                    </div>
                                </div>
                            )}
                        </div>

                        <div
                            onClick={() => handlerClick("fourth")}
                            onMouseOver={() => setСollaborativeRobotics(true)}
                            onMouseLeave={() => setСollaborativeRobotics(false)}
                        >
                            {collaborativeRobotics ? (
                                <div className="column_courses_white">
                                    <div className="up_text_column_courses_black">
                                        «Коллаборативная робототехника»
                                    </div>
                                    <div className="down_text_column_courses_black">
                                        уровень | Стартовый
                                    </div>
                                </div>
                            ) : (
                                <div className="column_courses_black">
                                    <div className="up_text_column_courses_white">
                                        «Коллаборативная робототехника»
                                    </div>
                                    <div className="down_text_column_courses_white">
                                        уровень | Стартовый
                                    </div>
                                </div>
                            )}
                        </div>

                        <div
                            onClick={() => handlerClick("fifth")}
                            onMouseOver={() =>
                                setСollaborativeRoboticsProgres(true)
                            }
                            onMouseLeave={() =>
                                setСollaborativeRoboticsProgres(false)
                            }
                        >
                            {collaborativeRoboticsProgres ? (
                                <div className="column_courses_white">
                                    <div className="up_text_column_courses_black">
                                        «Коллаборативная робототехника»
                                    </div>
                                    <div className="down_text_column_courses_black">
                                        уровень | продвинутый
                                    </div>
                                </div>
                            ) : (
                                <div className="column_courses_black">
                                    <div className="up_text_column_courses_white">
                                        «Коллаборативная робототехника»
                                    </div>
                                    <div className="down_text_column_courses_white">
                                        уровень | продвинутый
                                    </div>
                                </div>
                            )}
                        </div>
                    </div>
                </div>
            </div>
        </>
    );
};

export default Training;

    /*    <div className="container_education">
                        <div className="title_blocks_middle">обучение</div>
                        <div 
                            onMouseOver={() => setIsShowDefferentText(true)}
                            onMouseLeave={() => setIsShowDefferentText(false)}
                            className="text_row_middle">
                            Разработаны основные положения для обучения детей,
                            так и студентов высших учебных заведений и уже
                            готовых кадров в сфере автоматизации производства.
                            Для примера ниже показана блок схема приобретаемых
                            навыков после прохождения курса для детей от 6-9лет.
                        </div>
                    </div> */

