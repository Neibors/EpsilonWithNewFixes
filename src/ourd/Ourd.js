import React from "react";
import "./Ourd.css";
import imgworker from "../images/IMGWorker20Circle-min.png";

const Ourd = () => {
    return (
    <div className="container" id={'us'}>
    <div className="container_midle employee_row">
        <div className="box_item">
            <div className="title_blocks" >
                Наши Сотрудники
                
            </div>
            <div className="text_row">
            Нашими сотрудниками являются выпускники высших учебных заведений по престижным и востребованным на рынке направлениям: «Мехатроника и робототехника», «Механика и математическое моделирование», «Обработка металлов давлением», «Технологии современного сварочного производства» и др. Так, мы никогда не останавливаемся на достигнутом, а наши специалисты проходят курсы повышения квалификации от таких гигантов, как: OMRON, KUKA, Siemens, FANUK, Yaskawa и др. 

            </div>
        </div>
        <div className="box_item">
            <img src={imgworker} alt="employees" className="employee_img" />
        </div>
    </div>
</div>
    );
};

export default Ourd;