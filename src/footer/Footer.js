import React from "react";
import "./Footer.css";
import ENLang from "../images/ENLang-min.png";
import RULang from "../images/RULang-min.png";

const Footer = () => {
    return (
        <div className="footer">
            <div className="container">
                <div className="footer_row">
                    <div className="footer_text">Инициатор проекта: ООО «ЗИТ»<br /> При поддержке: института металлургии
                        УрО РАН </div>
                    <div className="lang_icons">
                        <img src={RULang} alt="RULang" className="lang-item" />
                        <img src={ENLang} alt="ENLang" className="lang-item" />
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Footer;