import React from "react";
// Import Swiper React components
import { Swiper, SwiperSlide} from "swiper/react";

// Import Swiper styles
import "swiper/css";
import "swiper/css/pagination";
import "swiper/css/navigation";

/* import "./styles.css"; */
import "./Openmip.css";
import bgrobot from "../images/BGRobot-min.png";
import { Autoplay, Pagination, Mousewheel, Navigation } from "swiper";

const Openmip = () => {
    return (
        <div className="container">
            <img src={bgrobot} alt="robot" className="robot_bg" />
            <div className="container_midle right_side">
                <div className="title_blocks">
                СОТРУДНИЧЕСТВО С УРО РАН
                </div>
                <div className="text_row static_right_text">
                Будущее стоит за Российской наукой и её развитием. Сотрудничество науки и производства позволяет находить новые решения, в том числе при разработке отечественных продуктов, превышающих по качеству зарубежные аналоги
                </div>
                <div className="text_row open_text">
                    <Swiper
                        spaceBetween={30}
                        centeredSlides={true}
                        autoplay={{
                            delay: 11500,
                            disableOnInteraction: false,
                        }}
                        pagination={{
                            clickable: true,
                        }}

                        mousewheel={{
                            invert: false,
                        }}
                        navigation={false}
                        modules={[Autoplay, Pagination, Mousewheel, Navigation]}
                        className="mySwiper" 
                    >
                        <SwiperSlide>
                        <div className="text_row open_text">
                        Мы сотрудничаем с Уральским Отделением Российской Академией Наук, в частности, с Институтом Металлургии, который является нашим акционером. 
                            </div>
                        </SwiperSlide>
                        <SwiperSlide>
                            <div className="text_row open_text">
                            Совместно с УРО РАН мы развиваем направления, связанные с роботами, нейронными сетями или искусственным интеллектом, которые считаются «краеугольными камнями» в вопросе развития импортозамещения.
                            </div>
                        </SwiperSlide>
                        <SwiperSlide>
                            <div className="text_row open_text">
                            Осуществляем дополнительную образовательную деятельность, которая благодаря привлечению студентов к реальным проектам, позволяет выпускать кадры, наиболее подготовленные к реальным условиям работы.
                            </div>
                        </SwiperSlide>
                    </Swiper>
                </div>
            </div>
        </div>
    );
};
export default Openmip;
