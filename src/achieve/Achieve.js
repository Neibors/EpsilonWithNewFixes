import React from "react";
import "./Achieve.css";
// import bgblock from "../images/1bgblock.png";


const Achieve = () => {
    return (
        <div className="container">
        <div className="container_midle flex_row_nav">
            <div className="left_text_row">

                <div className="title_blocks_middle">АО «ЭПСИЛОН»</div>
                <div className="text_row industry_text_row">
                АО «ЭПСИЛОН» занимается разработками в области информационных технологий, робототехники, узконаправленных процессов и сложного оборудования специального назначения.
                </div>
                <div className="text_row industry_text_row">
                Обладая квалифицированными штатом, высокотехнологичным оборудованием и накопленным опытом, АО «ЭПСИЛОН» способно решать самые сложные технологические задачи, а именно:
                </div>

            </div>

            <div className="petals">

            <div className="vertical__column_item img1">
                <div className="blur">
                    <div className="achive_culumn_text">
                    Проведение научно-исследовательских и опытно-конструкторских работ любой сложности
                    </div>
                </div> 
            </div>
            <div className="vertical__column_item img2">
                <div className="blur">
                    <div className="achive_culumn_text">
                    Разработка РТК, периферийных устройств и специализированного оборудования
                    </div>
                </div> 
            </div>
            <div className="vertical__column_item img3">
                <div className="blur">
                    <div className="achive_culumn_text">
                    Разработка программного обеспечения (MES, SCADA, ПО с аппаратной частью и т.д)
                    </div>
                </div> 
            </div>
            <div className="vertical__column_item img4">
                <div className="blur">
                    <div className="achive_culumn_text">
                    Интеграция РТК (внедрение, разноотраслевые решения, создание и инжиниринг производственных линий)
                    </div>
                </div> 
            </div>
{/* 
                <div className="vertical__column_item achive_culumn_text">
                Разработка РТК, периферийных устройств и специализированного оборудования
                </div>
                
                <div className="vertical__column_item achive_culumn_text">
                Разработка программного обеспечения (MES, SCADA, ПО с аппаратной частью и т.д)
                </div>

                <div className="vertical__column_item achive_culumn_text">
                Интеграция РТК (внедрение, разноотраслевые решения, создание и инжиниринг производственных линий)
                </div> */}
            </div>
        </div>
        </div>
    
    );
};

export default Achieve;