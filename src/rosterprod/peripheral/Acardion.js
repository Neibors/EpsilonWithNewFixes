import React from "react";
import { useState } from "react";


const Acardion = ({active, setActive}) => {
    const [lightFirst, setLightFirst] = useState(false);
    const [lightSecond, setLightSecond] = useState(false);
    const [lightThird, setLightThird] = useState(false);
    const [lightFourth, setLightFourth] = useState(false);
    const [lightFifth, setLightFifth] = useState(false);
    const [lightSixth, setLightSixth] = useState(false);
   

    return (       
        <div className={active ? "dropdown dropdownActive" : "dropdown"}>
            <div>
            <a
                href="https://drive.google.com/file/d/1Ew3KJbTVcrC9npMlIUfTaz_eeDx0kwYR/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightFirst(true)}
                onMouseLeave={() => setLightFirst(false)}
                className={lightFirst ? "prod_text prod_text_a" : "prod_text"}
            >
                Горелки под PTK RWM500 и RWT500
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/1IudF4Z5H2onWUMlUgSBGVfwhfj4YN_Ug/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightSecond(true)}
                onMouseLeave={() => setLightSecond(false)}
                className={lightSecond ? "prod_text prod_text_a" : "prod_text"}
            >
                Фланец быстросъёмный для инструментов РТК
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/10jQ0KOeay_esBQ9xdKSiiuqqamntOeHI/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightThird(true)}
                onMouseLeave={() => setLightThird(false)}
                className={lightThird ? "prod_text prod_text_a" : "prod_text"}
            >
                Магнитное основание для коллаборативных РТК
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/16HzpMUWNalh-LxskPhAuHNlE9gV-Tj9s/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightFourth(true)}
                onMouseLeave={() => setLightFourth(false)}
                className={lightFourth ? "prod_text prod_text_a" : "prod_text"}
            >
                Байонетное основание для коллаборативных РТК MAGFAS
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/16HzpMUWNalh-LxskPhAuHNlE9gV-Tj9s/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightFifth(true)}
                onMouseLeave={() => setLightFifth(false)}
                className={lightFifth ? "prod_text prod_text_a" : "prod_text"}
            >
                Болтовое основание для коллаборативных PTK BAS
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/1dLxz0f8DPHgYtGICgG3MVd3LbOnHRl-c/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightSixth(true)}
                onMouseLeave={() => setLightSixth(false)}
                className={lightSixth ? "prod_text prod_text_a" : "prod_text"}
            >
                Универсальные системы защиты для сварщиков
            </a>
            </div>
        </div>
    );
    };
    export default Acardion;