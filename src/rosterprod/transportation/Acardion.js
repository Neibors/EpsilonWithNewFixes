import React from "react";
import { useState } from "react";


const Acardion = ({active, setActive}) => {
    const [lightFirst, setLightFirst] = useState(false);
 

    return (       
        <div className={active ? "dropdown dropdownActive" : "dropdown"}>
            <div>
            <a
                href="https://drive.google.com/file/d/1s_Sf_dCX8tyxrv1tO9RQfTNRdb_br0Am/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightFirst(true)}
                onMouseLeave={() => setLightFirst(false)}
                className={lightFirst ? "prod_text prod_text_a" : "prod_text"}
            >
                Тележка для транспортировки коллаборативного РТК - WEELBOT
            </a>
            </div>
        </div>
    );
    };
    export default Acardion;