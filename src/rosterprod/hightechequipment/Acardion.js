import React from "react";
import { useState } from "react";

const Acardion = ({ active, setActive }) => {
    const [lightFirst, setLightFirst] = useState(false);
    const [lightSecond, setLightSecond] = useState(false);

    return (
        <div className={active ? "dropdown dropdownActive" : "dropdown"}>
            <div>
                <a
                    href="https://drive.google.com/file/d/1f7NEVfdRwZsjG7LOIKMG7lKNdsNY7u3v/view?usp=sharing"
                    target="newwindow"
                    title="Подробная информация о продукте"
                    onMouseOver={() => setLightFirst(true)}
                    onMouseLeave={() => setLightFirst(false)}
                    className={
                        lightFirst ? "prod_text prod_text_a" : "prod_text"
                    }
                >
                    Комплекс плазменной закалки МАК-10
                </a>
            </div>
            <div>
                <a
                    href="https://drive.google.com/file/d/1f7NEVfdRwZsjG7LOIKMG7lKNdsNY7u3v/view?usp=sharing"
                    target="newwindow"
                    title="Подробная информация о продукте"
                    onMouseOver={() => setLightSecond(true)}
                    onMouseLeave={() => setLightSecond(false)}
                    className={
                        lightSecond ? "prod_text prod_text_a" : "prod_text"
                    }
                >
                    Комплекс плазменной закалки МАК-100 (Анонс)
                </a>
            </div>
        </div>
    );
};
export default Acardion;
