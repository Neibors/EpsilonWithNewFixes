import React from "react";
import { useState } from "react";

const Acardion = ({ active, setActive }) => {
    const [lightFirst, setLightFirst] = useState(false);
    const [lightSecond, setLightSecond] = useState(false);
    const [lightThird, setLightThird] = useState(false);
    const [lightFourth, setLightFourth] = useState(false);
    const [lightFifth, setLightFifth] = useState(false);

    return (
        <div className={active ? "dropdown dropdownActive" : "dropdown"}>
            <div>
                <a
                    // href="#courserobotics"
                    target="newwindow"
                    title="Подробная информация о продукте"
                    onMouseOver={() => setLightFirst(true)}
                    onMouseLeave={() => setLightFirst(false)}
                    className={
                        lightFirst ? "prod_text prod_text_a" : "prod_text"
                    }
                >
                    Курс «Робототехника. Старт» для детей от 6-9 лет
                </a>
            </div>
            <div>
                <a
                    // href="#courseindustrialrobotics"
                    target="newwindow"
                    title="Подробная информация о продукте"
                    onMouseOver={() => setLightSecond(true)}
                    onMouseLeave={() => setLightSecond(false)}
                    className={
                        lightSecond ? "prod_text prod_text_a" : "prod_text"
                    }
                >
                    Курс «Промышленная робототехника. Старт»
                </a>
            </div>
            <div>
                <a
                    // href="#courseindustrialroboticsprogres"
                    target="newwindow"
                    title="Подробная информация о продукте"
                    onMouseOver={() => setLightThird(true)}
                    onMouseLeave={() => setLightThird(false)}
                    className={
                        lightThird ? "prod_text prod_text_a" : "prod_text"
                    }
                >
                    Курс «Промышленная робототехника. Продвинутый»
                </a>
            </div>
            <div>
                <a
                    // href="#coursecollaborativerobotics"
                    target="newwindow"
                    title="Подробная информация о продукте"
                    onMouseOver={() => setLightFourth(true)}
                    onMouseLeave={() => setLightFourth(false)}
                    className={
                        lightFourth ? "prod_text prod_text_a" : "prod_text"
                    }
                >
                    Курс «Коллаборативная робототехника. Старт»
                </a>
            </div>
            <div>
                <a
                    // href="#coursecollaborativeroboticsprogres"
                    target="newwindow"
                    title="Подробная информация о продукте"
                    onMouseOver={() => setLightFifth(true)}
                    onMouseLeave={() => setLightFifth(false)}
                    className={
                        lightFifth ? "prod_text prod_text_a" : "prod_text"
                    }
                >
                    Курс «Коллаборативная робототехника. Продвинутый»
                </a>
            </div>
        </div>
    );
};
export default Acardion;
