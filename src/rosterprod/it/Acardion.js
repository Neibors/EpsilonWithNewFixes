import React from "react";
import { useState } from "react";


const Acardion = ({active, setActive}) => {
    const [lightFirst, setLightFirst] = useState(false);
    const [lightSecond, setLightSecond] = useState(false);
    const [lightThird, setLightThird] = useState(false);
    const [lightFourth, setLightFourth] = useState(false);

    return (       
        <div className={active ? "dropdown dropdownActive" : "dropdown"}>
            <div>
            <a
                href="https://drive.google.com/file/d/1NUyI7JggXweZN70Oh_x0NBYPznBjivc-/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightFirst(true)}
                onMouseLeave={() => setLightFirst(false)}
                className={lightFirst ? "prod_text prod_text_a" : "prod_text"}
            >
                ReadWeld – программное обеспечение для контроля сварочных
                параметров
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/1nvaN8_5EftufpLT6zQBBSSRZJG3lX4tl/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightSecond(true)}
                onMouseLeave={() => setLightSecond(false)}
                className={lightSecond ? "prod_text prod_text_a" : "prod_text"}
            >
                Управляющее ПО для роботизированных РТК R-Weld
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/10jQ0KOeay_esBQ9xdKSiiuqqamntOeHI/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightThird(true)}
                onMouseLeave={() => setLightThird(false)}
                className={lightThird ? "prod_text prod_text_a" : "prod_text"}
            >
                Программное обеспечение для реализации сложных траекторий UPL
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/1V2rItR8XgXTk3CH6yUD-jJzwV8tLsBko/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightFourth(true)}
                onMouseLeave={() => setLightFourth(false)}
                className={lightFourth ? "prod_text prod_text_a" : "prod_text"}
            >
                Программное обеспечение для контроля скорости в режиме реального времени SPCH
            </a>
            </div>
        </div>
    );
    };
    export default Acardion;