import React from "react";

import iconpoin from "../../images/Icon_point_down-min.png";
import { useState } from "react";
import Acardion from "./Acardion";

/* import bgrobot from "../images/BGRobot.svg"; */
/* import { Autoplay, Pagination, Mousewheel, Navigation } from "swiper";
 */
const Modal = () => {
    /* 2 -сама компонента будет принимать несколько props  {active(отвечает за видимость компонента), setActive ( и функция которое это состояние изменяет)}*/
    const [modalActive, setModalActive] =
    useState(
        false
    ); /* 4 -Тут мы создаём состояние, то самое состояние отвечающее за видимость окна           -                      
4.1 -сделаем окно видимым по умолчанию
t-f*/
const [reverse, setReverse] = useState(false);
const [pointScale, setPointScale] = useState(false);
/* Тут мы создаём состояние отвечающее за видимость окна */
    return (
        <>
        <div className="point_row">
                <div className="title_point">ИТ</div>
                <div
                    onClick={() => setReverse(!reverse)}
                    onMouseOver={() => setPointScale(true)}
                    onMouseLeave={() => setPointScale(false)}
                >
                    <div className={reverse ? "up" : "down"}>
                        <img
                            src={iconpoin}
                            alt="Icon_point_down"
                            className={
                                pointScale
                                    ? "icon_point_down icon_point_scale"
                                    : "icon_point_down"
                            }
                            onClick={() => setModalActive(!modalActive)}
                        />
                        {/*  9 - тут у нас есть кнопка, вешаем слушатель нажатия  onClick={() => setModalActive(true)
                            и теперь при нажатии на неё у нас открывается модальное окно*/}
                    </div>
                </div>
            </div>
            <Acardion active={modalActive} setActive={setModalActive} />
       
        </>

    );
};
export default Modal;
