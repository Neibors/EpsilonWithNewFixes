import React from "react";
import { useState } from "react";


const Acardion = ({active, setActive}) => {
    const [lightFirst, setLightFirst] = useState(false);
    const [lightSecond, setLightSecond] = useState(false);
    const [lightThird, setLightThird] = useState(false);
    const [lightFourth, setLightFourth] = useState(false);
    const [lightFifth, setLightFifth] = useState(false);

    return (       
        <div className={active ? "dropdown dropdownActive" : "dropdown"}>
            <div>
            <a
                href="https://drive.google.com/file/d/1Nh70vcSV7jd5x9IDC0WhjnTitMGJSLuo/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightFirst(true)}
                onMouseLeave={() => setLightFirst(false)}
                className={lightFirst ? "prod_text prod_text_a" : "prod_text"}
            >
                RBOT 90
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/1Nh70vcSV7jd5x9IDC0WhjnTitMGJSLuo/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightSecond(true)}
                onMouseLeave={() => setLightSecond(false)}
                className={lightSecond ? "prod_text prod_text_a" : "prod_text"}
            >
                RBOT 900
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/1Nh70vcSV7jd5x9IDC0WhjnTitMGJSLuo/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightThird(true)}
                onMouseLeave={() => setLightThird(false)}
                className={lightThird ? "prod_text prod_text_a" : "prod_text"}
            >
                RBOT 900
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/1Nh70vcSV7jd5x9IDC0WhjnTitMGJSLuo/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightFourth(true)}
                onMouseLeave={() => setLightFourth(false)}
                className={lightFourth ? "prod_text prod_text_a" : "prod_text"}
            >
                Контроллер робота RDTM 500
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/1ZwZCy6BlSqcNZYIyzEu1j7tWlOdEuOnl/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightFifth(true)}
                onMouseLeave={() => setLightFifth(false)}
                className={lightFifth ? "prod_text prod_text_a" : "prod_text"}
            >
                Робот для обучения RBOT EDUCATION
            </a>
            </div>
        </div>
    );
    };
    export default Acardion;