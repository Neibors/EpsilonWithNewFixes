import React from "react";
import { useState } from "react";


const Acardion = ({active, setActive}) => {
    const [lightFirst, setLightFirst] = useState(false);
    const [lightSecond, setLightSecond] = useState(false);
    const [lightThird, setLightThird] = useState(false);


    return (       
        <div className={active ? "dropdown dropdownActive" : "dropdown"}>
            <div>
            <a
                href="https://drive.google.com/file/d/1C0UJ-bnJhEm9OBo70p1OvDTvbkdOIAq1/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightFirst(true)}
                onMouseLeave={() => setLightFirst(false)}
                className={lightFirst ? "prod_text prod_text_a" : "prod_text"}
            >
                Блок управления для комплекса РТК RWELD BOX
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/1C0UJ-bnJhEm9OBo70p1OvDTvbkdOIAq1/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightSecond(true)}
                onMouseLeave={() => setLightSecond(false)}
                className={lightSecond ? "prod_text prod_text_a" : "prod_text"}
            >
                Блок управления для контроля сварочных параметров REEDWELDBOX
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/1C0UJ-bnJhEm9OBo70p1OvDTvbkdOIAq1/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightThird(true)}
                onMouseLeave={() => setLightThird(false)}
                className={lightThird ? "prod_text prod_text_a" : "prod_text"}
            >
                Универсальный блок управления для реализации сварки РТК UBOX
            </a>
            </div>
        </div>
    );
    };
    export default Acardion;