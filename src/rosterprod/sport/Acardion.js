import React from "react";
import { useState } from "react";


const Acardion = ({active, setActive}) => {
    const [lightFirst, setLightFirst] = useState(false);
    const [lightSecond, setLightSecond] = useState(false);
    const [lightThird, setLightThird] = useState(false);


    return (       
        <div className={active ? "dropdown dropdownActive" : "dropdown"}>
            <div>
            <a
                href="https://drive.google.com/file/d/1E1gFeK63Iuc8Y3UUwlbmjHDg2H12xTJC/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightFirst(true)}
                onMouseLeave={() => setLightFirst(false)}
                className={lightFirst ? "prod_text prod_text_a" : "prod_text"}
            >
                Сенсорное снаряжение для фитнеса
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/1oqDzkc_9LHeURhY3-DTcqk1-vSo8Oar-/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightSecond(true)}
                onMouseLeave={() => setLightSecond(false)}
                className={lightSecond ? "prod_text prod_text_a" : "prod_text"}
            >
                Высококлассное судейское снаряжение
            </a>
            </div>
            <div>
            <a
                href="https://drive.google.com/file/d/1Ei2XKRyHOY89e9JSNskLKNBZTzlwA4ml/view?usp=sharing"
                target="newwindow"
                title = "Подробная информация о продукте"
                onMouseOver={() => setLightThird(true)}
                onMouseLeave={() => setLightThird(false)}
                className={lightThird ? "prod_text prod_text_a" : "prod_text"}
            >
                Мех. пейнтбольный пистолет LEPSOGUN
            </a>
            </div>
        </div>
    );
    };
    export default Acardion;