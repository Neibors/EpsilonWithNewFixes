import React from "react";
import microchip from "../images/microchip-min.png";
import It from "./it/Modal";
import Education from "./education/Modal";
import Sportsindustry  from "./sport/Modal";
import Robotech from "./robotech/Modal";
import Peripheral from "./peripheral/Modal";
import Control from "./control/Modal";
import Transportation from "./transportation/Modal";
import Hightechequipment from "./hightechequipment/Modal";


import { useState } from "react";

const Rosterprod = () => {
    const [itActive, setItActive] = useState(false);
    const [educationActive, setEducationActive] = useState(false);
    const [sportsindustryActive, setSportsindustryActive] = useState(false);
    const [robotechActive, setRobotechActive] = useState(false);
    const [peripheralActive, setPeripheralActive] = useState(false);
    const [transportationActive, setTransportationActive] = useState(false);
    const [controlActive, setControlActive] = useState(false);
    const [hightechequipmentActive, setHightechequipmentActive] = useState(false);
        return (
            
        <div className="container container_midle" id={'rosterprod'}>
            <div className="container_relative">
            <div>
            <img src={microchip} alt="microchip" className="microchip" />
       
                                <div className="title_blocks">
                                    Продукты предлагаемые к<br />
                                    коммерциализации
                                </div>
                            </div>
                            <It active={itActive} setActive={setItActive} />
                            <div className="line_unpoint"></div>
                            <Education active={educationActive} setActive={setEducationActive} />
                            <div className="line_unpoint"></div>
                            <Sportsindustry active={sportsindustryActive} setActive={setSportsindustryActive} />
                            <div className="line_unpoint"></div>
                            <Robotech active={robotechActive} setActive={setRobotechActive} />
                            <div className="line_unpoint"></div>
                            <Peripheral active={peripheralActive} setActive={setPeripheralActive} />
                            <div className="line_unpoint"></div>
                            <Transportation active={transportationActive} setActive={setTransportationActive} />
                            <div className="line_unpoint"></div>
                            <Control active={controlActive} setActive={setControlActive} />
                            <div className="line_unpoint"></div>
                            <Hightechequipment active={hightechequipmentActive} setActive={setHightechequipmentActive} />
                            <div className="line_unpoint"></div>
                            </div>
        </div>

    );
};

export default Rosterprod;
