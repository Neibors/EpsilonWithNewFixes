import React from "react";


const Acardion = ({active, setActive}) => {
    return (       
        <div className={active ? "dropdown dropdownActive" : "dropdown"}>
            <div className="text_row">
                <ul>
                    <li>&#8226; Умение работать с функциями ConveyorTrack</li>
                    <li>&#8226; Умение написания программы структурированным текстом, на основе script PolyScope</li>
                    <li>&#8226; Базовые понятия математических функций на основе script PolyScope</li>
                    <li>&#8226; Базовые умения программирования движений робота с помощью функций script PolyScope</li>
                    <li>&#8226; Базовые умения работы с плоскостями робота в виде структурированного текста, на основе script PolyScope</li>
                    <li>&#8226; Базовые умения работы с датчиком усилия в виде структурированного текста, на основе script PolyScope</li>
                    <li>&#8226; Базовые знания в настройке периферийных устройств, интерфейсов связи и т.д.</li>
                </ul>
            </div>
        </div>
    );
    };
    export default Acardion;