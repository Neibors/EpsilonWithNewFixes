import React from "react";


const Acardion = ({active, setActive}) => {
    return (       
        <div className={active ? "dropdown dropdownActive" : "dropdown"}>
            <div className="text_row">
                <ul>
                    <li>&#8226; Universal Robot UR5e</li>
                    <li>&#8226; Захват и пакеты расширений собственного производства и производителя OnRobot.</li>
                    <li>&#8226; Дополнительное оборудование</li>
                    <li>&#8226; ПЛК Siemens/Omron, оптические и концевые переключатели</li>
                </ul>
            </div>
        </div>
    );
    };
    export default Acardion;