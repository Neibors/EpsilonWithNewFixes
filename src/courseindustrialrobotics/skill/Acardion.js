import React from "react";


const Acardion = ({active, setActive}) => {
    return (       
        <div className={active ? "dropdown dropdownActive" : "dropdown"}>
            <div className="text_row">
                <ul>
                    <li>&#8226; Базовые понятия промышленной робототехники</li>
                    <li>&#8226; Базовые умения движения и программирования промышленного робота</li>
                    <li>&#8226; Базовые умения использования внешнего периферийного оборудования</li>
                    <li>&#8226; Настройки конфигурации юстировочных модулей</li>
                    <li>&#8226; Базовые знания логического управления, использование переменных и циклов</li>
                    <li>&#8226; Базовые знания внешней автоматики с использование ПЛК</li>
                </ul>
            </div>
        </div>
    );
    };
    export default Acardion;