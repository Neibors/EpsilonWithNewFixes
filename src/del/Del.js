import React from "react";
import "./Del.css";

const Del = () => {
    return (
        <div className="footer">
            <div className="container container_relative">
                <div className="flex_row_nav_cul">
                    <div className="foter centr prod_text_a">
                        Ver. alpha 0.0.2
                    </div>
                   
                </div>
            </div>
        </div>
    );
};

export default Del;
