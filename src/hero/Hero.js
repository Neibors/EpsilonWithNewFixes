import React from "react";
import "./Hero.css";
import epsilan_white_big from "../images/epsilanwhitelogo-min.png";

const Hero = () => {
    return (
        <div className="earth">
            <div className="container">
                <div className="title_row">
                    <div className="epsilan_white">
                        <img src={epsilan_white_big} alt="Epsilan-White" />
                    </div>
                    <div className="title_eps">
                        <div className="title_text">
                        Разработка и обучение в сфере робототехники и информационных технологий
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
};

export default Hero;
