import React from "react";
import "./Blockses.css";
import blocksgroup from "../images/21_Blocks_nobg-min.svg";

const Blockses = () => {
    return (
        <div className="bg_blocks_course">
                <div className="container container_midle">
                    <img src={blocksgroup} alt="21_Blocks" />
                </div>
            </div>
    );
};

export default Blockses;