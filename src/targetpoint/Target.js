import React from "react";
import "./Target.css";
import targetimg from "../images/head_target_img-min.png";

const Target = () => {
    return (
        <>
        <div class="container container_relative">
                <img src={targetimg} alt="robot" class="heand_toch" />
                <div class="container_midle">
                    <div class="title_blocks">
                    НАША МИССИЯ
                    </div>
                    <div class="target_culumn">
                        <div class="target_block">
                            <div class="white_dat"></div>
                            <div class="text_inline_column">
                            Развитие и коммерциализация Российских разработок в сфере информационных технологий и автоматизации производственных процессов
                            </div>
                        </div>
                        <div class="target_block">
                            <div class="white_dat"></div>
                            <div class="text_inline_column">
                            Развитие импортозамещения путем разработки и замены на Российском рынке товаров иностранного производства отечественными
                            </div>
                        </div>
                        <div class="target_block">
                            <div class="white_dat"></div>
                            <div class="text_inline_column">
                            Обучение робототехнике, математике, программированию детей, школьников, студентов и сотрудников предприятий
                            </div>
                        </div>
                        <div class="target_block">
                            <div class="white_dat"></div>
                            <div class="text_inline_column">
                            Внедрение роботизированных комплексов с использованием сложных технологических процессов на Российских предприятиях
                            </div>
                        </div>
                        {/* <div class="target_block">
                            <div class="white_dat"></div>
                            <div class="text_inline_column">
                                Развитие импортозамещения в России
                            </div>
                        </div> */}
                    </div>
                </div>
            </div>
        </>
    );
};

export default Target;
