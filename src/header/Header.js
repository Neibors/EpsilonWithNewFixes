import React from "react";
import "./Header.css";
import Epsilan_black_mini from "../images/EpsilanBlacklogo-min.png";
import ENLang from "../images/ENLang-min.png";
import RULang from "../images/RULang-min.png";

const Header = () => {
    return (
        <header className="header">
            <div className="container">
                <div className="header_row">
                    <div>
                        <img src={Epsilan_black_mini} alt="epsilanblack" />
                    </div>
                    <div className="flex_row_nav">
                        <a className="header_text_nov header-item" href="#us">
                            о нас
                        </a>
                        <a className="header_text_nov header-item" href="#education">курсы</a>
                        <a href="#rosterprod" className="header_text_nov header-item">
                            продукты
                        </a>
                        <a href="#address" className="header_text_nov header-item">
                            контакты
                        </a>
                    </div>
                    <div className="lang_icons">
                        <img
                            src={RULang}
                            alt="RULang"
                            className="lang-item"
                        />
                        <img
                            src={ENLang}
                            alt="ENLang"
                            className="lang-item"
                        />
                    </div>
                </div>
            </div>
        </header>
    );
};


export default Header;
