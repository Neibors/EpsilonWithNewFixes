import Header from "./header/Header";
import Hero from "./hero/Hero";
import Industrialization from "./industrialization/Industrialization";
import Openmip from "./openmip/Openmip";
import Ourd from "./ourd/Ourd";
import Achieve from "./achieve/Achieve";
import Target from "./targetpoint/Target";
import Trainingbutt from "./trainingbutt/Trainingbutt";
import Blockses from "./blockses/Blockses";
import Rosterprod from "./rosterprod/Rosterprod";
import Inlinerobots from "./inlinerobots/Inlinerobots";
import Address from "./address/Address";
// import Footer from "./footer/Footer"; sdkjcndsaklcndsiсмт
import Courserobotics from "./courserobotics/Courserobotics";
import Courseindustrialrobotics from "./courseindustrialrobotics/Courseindustrialrobotics";
import Courseindustrialroboticsprogres from "./courseindustrialroboticsprogres/Courseindustrialroboticsprogres";
import Coursecollaborativerobotics from "./coursecollaborativerobotics/Coursecollaborativerobotics";
import Coursecollaborativeroboticsprogres from "./coursecollaborativeroboticsprogres/Coursecollaborativeroboticsprogres";
import Educationup from "./educationup/Educationup"
import Del from "./del/Del";
import "./App.css";
import { useState } from "react";

function App() {


    // const [first, setFirst] = useState(false);
    // const [second, setSecond] = useState(false);
    // const [third, setThird] = useState(false);
    // const [fourth, setFourth] = useState(false);
    // const [fifth, setFifth] = useState(false);
    const [selectedValue, setSelectedValue] = useState('first'); 
    
    // myCountryIsRusssia  -  camelCase пояснения
    // PascalCase - MyCountryIsAvesome пояснения
    // snake_case - my_city_is_perfect пояснения

    const handleChange = (mySelectedValue) => {
        setSelectedValue(mySelectedValue);
    };

    return (
        <div className="App">
            <Header />
            <Hero />
            <Industrialization />
            <Achieve />
            <Openmip />
            <Ourd />
            <Target />
            <Educationup />
           <Trainingbutt onChange={handleChange} /> 
            {selectedValue === 'first' && <Courserobotics />}
            {selectedValue === 'second' && <Courseindustrialrobotics />}
            {selectedValue === 'third' && <Courseindustrialroboticsprogres />}
            {selectedValue === 'fourth' && <Coursecollaborativerobotics />}
            {selectedValue === 'fifth' && <Coursecollaborativeroboticsprogres />} 
            <Blockses /> 
            <Rosterprod />
            <Inlinerobots />
            <Address />
            {/* <Footer />    */}
            <Del /> 
        </div>
    );
};

export default App;
