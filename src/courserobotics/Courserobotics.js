import React from "react";
import { useState } from "react"; /* Хук состояния */
import "./Courserobotics.css";
import bgcrob from "../images/3BGRobot-min.png";
/* import Use from "./use/Modal"; */
import Skill from './skill/Modal'

const Courserobotics = () => {
   /*  const [useActive, setUseActive] =
        useState(
            false
        ); */ /* 4 -Тут мы создаём состояние, то самое состояние отвечающее за видимость окна           -                      
    4.1 -сделаем окно видимым по умолчанию
    t-f*/
   const [skillActive, setSkillActive] =
   useState(false);
    /* Тут мы создаём состояние отвечающее за видимость окна */
    return (
        <div className="container container_midle container_relative">
            <img src={bgcrob} alt="robot" className="bg_1_robot_contur" />
            <div>
                <div className="title_courses">КУРС</div>
                <div className="title_blocks">
                    «Робототехника» для детей от 6-9 лет
                </div>
                <div className="title_courses">уровень | Стартовый</div>
                <div className="text_row left_container_courses">
                    Цель программы: развитие дивергентного мышления, творческих
                    способностей, навыков созидательной деятельности, развитие
                    социальных навыков и навыков работы в команде. Знакомство с
                    основами программирования на примере Lego WeDo 2.0, навыкам
                    создания своих собственных проектов и решения
                    алгоритмических задач, а также умение презентовать свой
                    проект. Общее количество часов 62, из них 23 теоретических,
                    49 практических, по 4 человека в группе.
                </div>
            </div>

         {/*    <Use active={useActive} setActive={setUseActive} />
            {/* 3 - Возвращаемся к веб компоненту и добовляем сюда модальное окно 
                
                5 - Передаём этой переменной функцию изменяющюю её как пропсы в модальное окно из пункта 2*/}
            {/* <div className="line_unpoint"></div> */}
 
            <Skill active={skillActive} setActive={setSkillActive} />
            <div className="line_unpoint"></div>
        </div>
    );
};

export default Courserobotics;
