import React from "react";


const Acardion = ({active, setActive}) => {
    return (       
        <div className={active ? "dropdown dropdownActive" : "dropdown"}>
            <div className="text_row">
                <ul>
                    <li>&#8226; KUKA Agilus/Cybertech</li>
                    <li>&#8226; Захват и пакет расширений GripperT ech</li>
                    <li>&#8226; Дополнительное оборудование</li>
                    <li>&#8226; ПЛК Siemens/Omron</li>
                </ul>
            </div>
        </div>
    );
    };
    export default Acardion;