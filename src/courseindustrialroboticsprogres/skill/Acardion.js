import React from "react";


const Acardion = ({active, setActive}) => {
    return (       
        <div className={active ? "dropdown dropdownActive" : "dropdown"}>
            <div className="text_row">
                <ul>
                    <li>&#8226; Базовые понятия среды разработки WorkVisual</li>
                    <li>&#8226; Основы программирования в WorkVisual</li>
                    <li>&#8226; Использование режима Expert, работы с различными типами данных</li>
                    <li>&#8226; Умение использовать подпрограммы и их функции</li>
                    <li>&#8226; Использование пользовательских сообщений и движений в KRL</li>
                    <li>&#8226; Использование расширений на основе KukaArcTech, перенастройка, введение в эксплуатацию</li>
                </ul>
            </div>
        </div>
    );
    };
    export default Acardion;