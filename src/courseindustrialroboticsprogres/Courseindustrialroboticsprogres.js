import React from "react";
import "./Courseindustrialroboticsprogres.css";
import bgcrob from "../images/2BGRobot-min.png";
import Use from "./use/Modal";
import Skill from "./skill/Modal";
import { useState } from "react";

const Courseindustrialroboticsprogres = () => {
    const [useActive, setUseActive] =
        useState(
            false
        ); /* 4 -Тут мы создаём состояние, то самое состояние отвечающее за видимость окна           -                      
4.1 -сделаем окно видимым по умолчанию
t-f*/
    const [skillActive, setSkillActive] = useState(false);
    return (
        <div className="container container_midle container_relative">
            <img src={bgcrob} alt="robot" className="bg_2_robot_contur" />
            <div>
                <div className="title_courses">КУРС</div>
                <div className="title_blocks">«Промышленная робототехника»</div>
                <div className="title_courses">уровень | продвинутый</div>
                <div className="text_row left_container_courses">
                    Программа предназначена для слушателей 17-30 лет ( в первую очередь для учащихся выпускных классов и студентов младших курсов) Курс рассчитан в первую очередь на роботов производителя KUKA версии KR C4-C5 Продолжительность курса: 20 часов. Большая часть курса состоит из наглядных презентаций и «живых» демонстраций робота. Курс рассчитан на количество человек, кратное количеству ячеек.

                </div>
            </div>
            <Use active={useActive} setActive={setUseActive} />
            <div className="line_unpoint"></div>
            <Skill active={skillActive} setActive={setSkillActive} />
            <div className="line_unpoint"></div>
        </div>
    );
};

export default Courseindustrialroboticsprogres;
